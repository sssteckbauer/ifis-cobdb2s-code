000100 IDENTIFICATION DIVISION.                                         00010000
       PROGRAM-ID. FGIO03.
000300 AUTHOR. CHRISTOPHER C. SCHAUER                                   00030000
000400 DATE-WRITTEN.  MARCH 1989.                                       00040000
000500****************************************************************  00050000
000600*REMARKS.                                                         00060000
000700*    THIS PROGRAM IS CALLED TO GET THE USER-NAME.  CALLING        00070000
000800* PROGRAM SHOULD USE FGIO03CB COPY BOOK MEMBER AND FILL IN        00080000
000900* UNVERSITY AND USER-ID FIELDS.                                   00090000
001000* CHANGES:                                                        00100000
001100* 05/01/91 - HAC CT-202 - RELEASE 1.60                            00110000
001200*    NEED TO RECOMPILE DUE TO CHANGE IN COPYBOOK MEMBERS          00120000
001MLJ* 08/24/11  DEVMLJ  DO NOT LOOK WHEN USER-ID IS UUTU200   FP3561
001300****************************************************************  00130000
      *%****************************************************************
      *%                                                               *
      *%  Converted using Forecross Corporation Redeployment Software  *
      *%                                                               *
      *%               Convert/IDMS-DB Version 8 Release 01            *
      *%                                                               *
      *%                Converted on 10/06/00 at 12:48                 *
      *%                                                               *
      *%****************************************************************
      *%              COPYRIGHT FORECROSS CORPORATION                  *
      *%                2000  ALL RIGHTS RESERVED                      *
      *%****************************************************************
           COPY CBCVDBNT.

      /
      *%-------------------------------------------------------------- *
      *%
      *%  *** TABLE VIEWS USED IN THIS PROGRAM
      *%
      *%YUS_SEC_USR_V         R8005                            CBL CPY
      *%
      *%  *** CURSORS DECLARED IN THIS PROGRAM
      *%
      *%
      *%  *** SUBROUTINES GENERATED FOR THIS PROGRAM
      *%
      *%000-SQL-INITIALIZATION
      *%986-RESET-KEYS
      *%906-05-R8005
      *%985-CURR-R8005
      *%996-COMMIT
      *%
      *%-------------------------------------------------------------- *
001400 ENVIRONMENT DIVISION.                                            00140000
001500*IDMS-CONTROL SECTION.                                            00150000
001600*PROTOCOL.  MODE IS BATCH DEBUG.                                  00160000
001700*%          IDMS-RECORDS MANUAL.                                  00170000
001800****************************************************************  00180000
001900 DATA DIVISION.                                                   00190000
002000*SCHEMA SECTION.                                                  00200000
002100*DB FSMSBA03 WITHIN SCTYNTWK.                                     00210000
002200*DB FIMSNWKB WITHIN FIMSNTWN.                                     00220000
002300****************************************************************  00230000

002400 WORKING-STORAGE SECTION.                                         00240000
002500 COPY WSSTRTWS.                                                   00250000
           COPY BTCHWS.
002600*01  COPY IDMS SUBSCHEMA-CONTROL.                                 00260000
       01  DBLINK-USER-CD   PIC X(8) VALUE 'FGIO03.'.
       01  SUBSCHEMA-CONTROL  PIC X.

      *%****************************** *
      *% YUS_SEC_USR_V DATA VIEW AREAS
      *%****************************** *
      *%   COPY CBID8005.
CPYNME     COPY R8005.
002700*01  COPY IDMS RECORD R8005.                                      00270000
002800 COPY WSDONEWS.                                                   00280000
002900****************************************************************  00290000
UCSD1      EXEC SQL
UCSD1          INCLUDE HVMSL
UCSD1      END-EXEC.
UCSD1 *%   EXEC SQL
UCSD1 *%       INCLUDE ACMSGHT
CPYNME     COPY ACMSGHT.
UCSD1 *%   END-EXEC.
           EXEC SQL
               INCLUDE CBCVDBCA
           END-EXEC.

      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%          STANDARD CONVERT/DB COMMUNICATION AREAS              *
      *%                                                               *
      *%-------------------------------------------------------------- *
           COPY CBCVDBXD.
       01  DBLINK-NAMES.
           05  DBLINK-SCHEMA-NAME.
               10  FILLER                   PIC X(8)
                       VALUE 'FIMSNTWN'.
           05  DBLINK-RECORD-NAMES.
               10  DBLINK-R4000-COA         PIC X(16)
                       VALUE 'R4000-COA'.
               10  DBLINK-R4001-FUND        PIC X(16)
                       VALUE 'R4001-FUND'.
               10  DBLINK-R4002-ORGN        PIC X(16)
                       VALUE 'R4002-ORGN'.
               10  DBLINK-R4003-ACCT        PIC X(16)
                       VALUE 'R4003-ACCT'.
               10  DBLINK-R4004-PROG        PIC X(16)
                       VALUE 'R4004-PROG'.
               10  DBLINK-R4006-LCTN        PIC X(16)
                       VALUE 'R4006-LCTN'.
               10  DBLINK-R4008-FNDT        PIC X(16)
                       VALUE 'R4008-FNDT'.
               10  DBLINK-R4011-SYSDAT      PIC X(16)
                       VALUE 'R4011-SYSDAT'.
               10  DBLINK-R4014-ACTT        PIC X(16)
                       VALUE 'R4014-ACTT'.
               10  DBLINK-R4016-BDGT-CTRL   PIC X(16)
                       VALUE 'R4016-BDGT-CTRL'.
               10  DBLINK-R4021-CTRL-ACCTS  PIC X(16)
                       VALUE 'R4021-CTRL-ACCTS'.
               10  DBLINK-R4026-INDRT-COST  PIC X(16)
                       VALUE 'R4026-INDRT-COST'.
               10  DBLINK-R4030-COST-SHARE  PIC X(16)
                       VALUE 'R4030-COST-SHARE'.
               10  DBLINK-R4033-BANK        PIC X(16)
                       VALUE 'R4033-BANK'.
               10  DBLINK-R4035-PAY-MTHD    PIC X(16)
                       VALUE 'R4035-PAY-MTHD'.
               10  DBLINK-R4036-USER-RCRD   PIC X(16)
                       VALUE 'R4036-USER-RCRD'.
               10  DBLINK-R4038-USER-CODES  PIC X(16)
                       VALUE 'R4038-USER-CODES'.
               10  DBLINK-R4040-CLOSE-HDR   PIC X(16)
                       VALUE 'R4040-CLOSE-HDR'.
               10  DBLINK-R4045-ACTI        PIC X(16)
                       VALUE 'R4045-ACTI'.
               10  DBLINK-R4046-BANK-NAME   PIC X(16)
                       VALUE 'R4046-BANK-NAME'.
               10  DBLINK-R4050-ACTV        PIC X(16)
                       VALUE 'R4050-ACTV'.
               10  DBLINK-R4052-XTRN-ENTY   PIC X(16)
                       VALUE 'R4052-XTRN-ENTY'.
               10  DBLINK-R4053-INTR-ENTY   PIC X(16)
                       VALUE 'R4053-INTR-ENTY'.
               10  DBLINK-R4054-XTRN-CODE   PIC X(16)
                       VALUE 'R4054-XTRN-CODE'.
               10  DBLINK-R4056-AGNC        PIC X(16)
                       VALUE 'R4056-AGNC'.
               10  DBLINK-R4057-RQST-ID     PIC X(16)
                       VALUE 'R4057-RQST-ID'.
               10  DBLINK-R4060-RPT         PIC X(16)
                       VALUE 'R4060-RPT'.
               10  DBLINK-R4064-CS-EFCTV    PIC X(16)
                       VALUE 'R4064-CS-EFCTV'.
               10  DBLINK-R4065-IC-EFCTV    PIC X(16)
                       VALUE 'R4065-IC-EFCTV'.
               10  DBLINK-R4069-JRNL-HDR    PIC X(16)
                       VALUE 'R4069-JRNL-HDR'.
               10  DBLINK-R4070-RQST        PIC X(16)
                       VALUE 'R4070-RQST'.
               10  DBLINK-R4071-RQST-DTL    PIC X(16)
                       VALUE 'R4071-RQST-DTL'.
               10  DBLINK-R4072-BUYER       PIC X(16)
                       VALUE 'R4072-BUYER'.
               10  DBLINK-R4073-VENDOR      PIC X(16)
                       VALUE 'R4073-VENDOR'.
               10  DBLINK-R4074-CMDTY       PIC X(16)
                       VALUE 'R4074-CMDTY'.
               10  DBLINK-R4081-AGRMNT      PIC X(16)
                       VALUE 'R4081-AGRMNT'.
               10  DBLINK-R4083-PO-HDR      PIC X(16)
                       VALUE 'R4083-PO-HDR'.
               10  DBLINK-R4084-PO-CLASS    PIC X(16)
                       VALUE 'R4084-PO-CLASS'.
               10  DBLINK-R4085-PO-DTL      PIC X(16)
                       VALUE 'R4085-PO-DTL'.
               10  DBLINK-R4088-BID         PIC X(16)
                       VALUE 'R4088-BID'.
               10  DBLINK-R4089-BID-DTL     PIC X(16)
                       VALUE 'R4089-BID-DTL'.
               10  DBLINK-R4093-RCVNG-HDR   PIC X(16)
                       VALUE 'R4093-RCVNG-HDR'.
               10  DBLINK-R4096-PO-HDR-SEQ  PIC X(16)
                       VALUE 'R4096-PO-HDR-SEQ'.
               10  DBLINK-R4099-RETURNS     PIC X(16)
                       VALUE 'R4099-RETURNS'.
               10  DBLINK-R4104-VENDOR-CMD  PIC X(16)
                       VALUE 'R4104-VENDOR-CMD'.
               10  DBLINK-R4106-AUTO-JRNL   PIC X(16)
                       VALUE 'R4106-AUTO-JRNL'.
               10  DBLINK-R4120-TXT-HDR     PIC X(16)
                       VALUE 'R4120-TXT-HDR'.
               10  DBLINK-R4150-INV-HDR     PIC X(16)
                       VALUE 'R4150-INV-HDR'.
               10  DBLINK-R4151-INV-DTL     PIC X(16)
                       VALUE 'R4151-INV-DTL'.
               10  DBLINK-R4157-CHK-RQST-H  PIC X(16)
                       VALUE 'R4157-CHK-RQST-H'.
               10  DBLINK-R4158-CHK-HDR     PIC X(16)
                       VALUE 'R4158-CHK-HDR'.
               10  DBLINK-R4161-DSCNT-TBLE  PIC X(16)
                       VALUE 'R4161-DSCNT-TBLE'.
               10  DBLINK-R4163-RSBLE-INV   PIC X(16)
                       VALUE 'R4163-RSBLE-INV'.
               10  DBLINK-R4166-VNDR-TYPE   PIC X(16)
                       VALUE 'R4166-VNDR-TYPE'.
               10  DBLINK-R4168-TAX-RATES   PIC X(16)
                       VALUE 'R4168-TAX-RATES'.
               10  DBLINK-R4174-TOF-HDR     PIC X(16)
                       VALUE 'R4174-TOF-HDR'.
               10  DBLINK-R4180-RULE-CLASS  PIC X(16)
                       VALUE 'R4180-RULE-CLASS'.
               10  DBLINK-R4181-RULE-EFCTV  PIC X(16)
                       VALUE 'R4181-RULE-EFCTV'.
               10  DBLINK-R4183-ENCMBR-YRS  PIC X(16)
                       VALUE 'R4183-ENCMBR-YRS'.
               10  DBLINK-R4187-G-FOAPAL    PIC X(16)
                       VALUE 'R4187-G-FOAPAL'.
               10  DBLINK-R4188-TRAN-HIST   PIC X(16)
                       VALUE 'R4188-TRAN-HIST'.
               10  DBLINK-R4190-ENCMBR-DTL  PIC X(16)
                       VALUE 'R4190-ENCMBR-DTL'.
               10  DBLINK-R4191-ENCMBR-HDR  PIC X(16)
                       VALUE 'R4191-ENCMBR-HDR'.
               10  DBLINK-R4192-USER-PRFL   PIC X(16)
                       VALUE 'R4192-USER-PRFL'.
               10  DBLINK-R4197-GNRL-LDGR   PIC X(16)
                       VALUE 'R4197-GNRL-LDGR'.
               10  DBLINK-R4199-ACCT-LDGR   PIC X(16)
                       VALUE 'R4199-ACCT-LDGR'.
               10  DBLINK-R4203-DOCUMENT    PIC X(16)
                       VALUE 'R4203-DOCUMENT'.
               10  DBLINK-R4204-APRVL-J     PIC X(16)
                       VALUE 'R4204-APRVL-J'.
               10  DBLINK-R4205-APRVL-LVL   PIC X(16)
                       VALUE 'R4205-APRVL-LVL'.
               10  DBLINK-R4209-CLAUSE      PIC X(16)
                       VALUE 'R4209-CLAUSE'.
               10  DBLINK-R4212-APRVL-HDR   PIC X(16)
                       VALUE 'R4212-APRVL-HDR'.
               10  DBLINK-R4215-APRV-TMPLT  PIC X(16)
                       VALUE 'R4215-APRV-TMPLT'.
               10  DBLINK-R4224-DLVRY-HDR   PIC X(16)
                       VALUE 'R4224-DLVRY-HDR'.
               10  DBLINK-R4226-EXPR-VNDR   PIC X(16)
                       VALUE 'R4226-EXPR-VNDR'.
               10  DBLINK-R4228-FILE-CNTL   PIC X(16)
                       VALUE 'R4228-FILE-CNTL'.
               10  DBLINK-R4229-RLSE-HDR    PIC X(16)
                       VALUE 'R4229-RLSE-HDR'.
               10  DBLINK-R4230-RLSE-DTL    PIC X(16)
                       VALUE 'R4230-RLSE-DTL'.
               10  DBLINK-R4232-AP-TRANS    PIC X(16)
                       VALUE 'R4232-AP-TRANS'.
               10  DBLINK-R4300-AST         PIC X(16)
                       VALUE 'R4300-AST'.
               10  DBLINK-R4400-TRVL        PIC X(16)
                       VALUE 'R4400-TRVL'.
               10  DBLINK-R4401-EVENT       PIC X(16)
                       VALUE 'R4401-EVENT'.
               10  DBLINK-R4402-TRIP        PIC X(16)
                       VALUE 'R4402-TRIP'.
               10  DBLINK-R4405-EXPNS       PIC X(16)
                       VALUE 'R4405-EXPNS'.
               10  DBLINK-R4406-RECON       PIC X(16)
                       VALUE 'R4406-RECON'.
               10  DBLINK-R4410-TRVL-INV    PIC X(16)
                       VALUE 'R4410-TRVL-INV'.
               10  DBLINK-R6000-SYS         PIC X(16)
                       VALUE 'R6000-SYS'.
               10  DBLINK-R6001-UNVRS       PIC X(16)
                       VALUE 'R6001-UNVRS'.
               10  DBLINK-R6117-PRSN        PIC X(16)
                       VALUE 'R6117-PRSN'.
               10  DBLINK-R6151-STATE       PIC X(16)
                       VALUE 'R6151-STATE'.
               10  DBLINK-R6152-ZIP         PIC X(16)
                       VALUE 'R6152-ZIP'.
               10  DBLINK-R6153-COUNTRY     PIC X(16)
                       VALUE 'R6153-COUNTRY'.
               10  DBLINK-R6200-ACH-DATA    PIC X(16)
                       VALUE 'R6200-ACH-DATA'.
               10  DBLINK-R6257             PIC X(16)
                       VALUE 'R6257'.
               10  DBLINK-R6258             PIC X(16)
                       VALUE 'R6258'.
               10  DBLINK-R6259             PIC X(16)
                       VALUE 'R6259'.
               10  DBLINK-R6260             PIC X(16)
                       VALUE 'R6260'.
               10  DBLINK-R6268             PIC X(16)
                       VALUE 'R6268'.
               10  DBLINK-R6270             PIC X(16)
                       VALUE 'R6270'.
               10  DBLINK-R6273             PIC X(16)
                       VALUE 'R6273'.
               10  DBLINK-R6311-ENTY        PIC X(16)
                       VALUE 'R6311-ENTY'.
               10  DBLINK-R6313-PRSN-CHG    PIC X(16)
                       VALUE 'R6313-PRSN-CHG'.
               10  DBLINK-R6353-ENTY-CHG    PIC X(16)
                       VALUE 'R6353-ENTY-CHG'.
               10  DBLINK-R6600-TABL-HDR    PIC X(16)
                       VALUE 'R6600-TABL-HDR'.
               10  DBLINK-R6605             PIC X(16)
                       VALUE 'R6605'.
               10  DBLINK-R6606             PIC X(16)
                       VALUE 'R6606'.
               10  DBLINK-R8001             PIC X(16)
                       VALUE 'R8001'.
               10  DBLINK-R8002             PIC X(16)
                       VALUE 'R8002'.
               10  DBLINK-R8003             PIC X(16)
                       VALUE 'R8003'.
               10  DBLINK-R8005             PIC X(16)
                       VALUE 'R8005'.
               10  DBLINK-R8007             PIC X(16)
                       VALUE 'R8007'.
               10  DBLINK-R8008             PIC X(16)
                       VALUE 'R8008'.
               10  DBLINK-R8009             PIC X(16)
                       VALUE 'R8009'.
               10  DBLINK-R8010             PIC X(16)
                       VALUE 'R8010'.
               10  DBLINK-R8013             PIC X(16)
                       VALUE 'R8013'.
               10  DBLINK-R8020             PIC X(16)
                       VALUE 'R8020'.
               10  DBLINK-R8030             PIC X(16)
                       VALUE 'R8030'.
               10  DBLINK-R8040             PIC X(16)
                       VALUE 'R8040'.
               10  DBLINK-R8044             PIC X(16)
                       VALUE 'R8044'.
               10  DBLINK-R8050             PIC X(16)
                       VALUE 'R8050'.
               10  DBLINK-R8051             PIC X(16)
                       VALUE 'R8051'.
               10  DBLINK-R8052             PIC X(16)
                       VALUE 'R8052'.
           05  DBLINK-SET-NAMES.
               10  FILLER                   PIC X(1)        VALUE SPACE.
           05  DBLINK-AREA-NAMES.
               10  F-SCTY                   PIC X(16)
                       VALUE 'F-SCTY'.
           05  DBLINK-RECORD-LOCK-FLAGS.
               10  FILLER                   PIC X(1)        VALUE SPACE.
           05  DBLINK-CURSOR-NAMES.
               10  FILLER                   PIC X(1)        VALUE SPACE.
           05  DBLINK-CURSOR-CODES.
               10  FILLER                   PIC X(1)        VALUE SPACE.
           02  DBLINK-KEYS.
           03  DBLINK-RECORD-KEYS.
           05  DBLINK-R8005-KEY.
               10  DBLINK-R8005-01          PIC X(8)        VALUE SPACE.
           03  DBLINK-SET-KEYS.
               10  FILLER                   PIC X(1)        VALUE SPACE.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  CONTROL AREAS AND VIEWS                      *
      *%                                                               *
      *%-------------------------------------------------------------- *

      *%****************************** *
      *%CONVERT/DB COMMAREA
      *%****************************** *
           EXEC SQL
               INCLUDE SQLCA
           END-EXEC.

      *%****************************** *
      *%YUS_SEC_USR_V AREAS(HOST VAR INCLUDE)
      *%****************************** *
      *%   EXEC SQL
      *%       INCLUDE CBHV8005
CPYNME     EXEC SQL
CPYNME         INCLUDE HVYUS
CPYNME     END-EXEC.
      *%   END-EXEC.
003000 LINKAGE SECTION.                                                 00300000
003100 COPY FGIO03CB.                                                   00310000
003200****************************************************************  00320000


003300 PROCEDURE DIVISION USING FGIO03CB.                               00330000
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%              000-SQL-INITIALIZATION ROUTINE                   *
      *%                                                               *
      *%-------------------------------------------------------------- *
       000-SQL-INITIALIZATION.
           MOVE '000-SQL-INITIALIZATION' TO DBLINK-CURR-PARAGRAPH.
           ACCEPT DBLINK-RUN-DATE-2 FROM DATE.
           MOVE DBLINK-RUN-DATE-2-MO TO DBLINK-RUN-DATE-MO.
           MOVE DBLINK-RUN-DATE-2-DY TO DBLINK-RUN-DATE-DY.
           MOVE DBLINK-RUN-DATE-2-YR TO DBLINK-RUN-DATE-YR.
           ACCEPT DBLINK-TIME FROM TIME.
           MOVE DBLINKX-NULL-DB2-DATE-ISO TO DBLINK-DEFAULT-DATE.
           MOVE '0001-01-01' TO DBLINK-LOW-DATE.
           MOVE '9999-12-31' TO DBLINK-HIGH-DATE.
           MOVE DBLINKX-NULL-DB2-TIME-ISO TO DBLINK-DEFAULT-TIME.
           MOVE '00.00.01' TO DBLINK-LOW-TIME.
           MOVE '23.59.59' TO DBLINK-HIGH-TIME.
       000-SQL-INITIALIZATION-EXIT.
           EXIT.

003400****************************************************************  00340000
003500*%   BIND RUN-UNIT.                                               00350000
           MOVE 0001 TO DBLINK-CURR-IO-NUM
           MOVE +0 TO SQLCODE
           MOVE ZERO TO ERROR-STATUS
           PERFORM 986-RESET-KEYS
              THRU 986-RESET-KEYS-EXIT.
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
003700*%   BIND R8005.                                                  00370000
           MOVE 0002 TO DBLINK-CURR-IO-NUM
           MOVE +0 TO SQLCODE
           MOVE ZERO TO ERROR-STATUS.
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
003900*%   READY F-SCTY          USAGE-MODE IS RETRIEVAL.               00390000
           MOVE 0003 TO DBLINK-CURR-IO-NUM
           MOVE +0 TO SQLCODE
           MOVE ZERO TO ERROR-STATUS.
           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
004100     MOVE FGIO03-UNVRS-CODE TO UNVRS-CODE-8005.                   00410000
004200     MOVE FGIO03-USER-ID    TO SCRTY-USER-CODE-8005.              00420000
004300*%   OBTAIN CALC R8005.                                           00430000
           MOVE 0004 TO DBLINK-CURR-IO-NUM
           MOVE DBLINK-OBTAIN TO DBLINK-TYPE-SELECT
           MOVE DBLINK-CALC TO DBLINK-TYPE-GET
           MOVE SCRTY-USER-CODE-8005 OF R8005 TO YUS-SCRTY-USER-CD
           PERFORM 906-05-R8005
              THRU 906-05-R8005-EXIT.
001MLJ     IF FGIO03-USER-ID = 'UUTU200'
  |          MOVE SPACES TO FGIO03-USER-NAME
001MLJ     ELSE
004400     IF DB-REC-NOT-FOUND                                          00440000
004500         MOVE '*** USER NOT FOUND IN SECURITY ***' TO             00450000
004600           FGIO03-USER-NAME                                       00460000
004700     ELSE                                                         00470000
004800         IF DB-STATUS-OK                                          00480000
004900             MOVE FULL-NAME-8005 TO FGIO03-USER-NAME              00490000
005000         ELSE                                                     00500000
                   PERFORM 999-SQL-STATUS
                      THRU 999-SQL-STATUS-EXIT.
005200*%   FINISH.                                                      00520000
           MOVE 0005 TO DBLINK-CURR-IO-NUM
           MOVE +0 TO SQLCODE
           MOVE ZERO TO ERROR-STATUS
           PERFORM 986-RESET-KEYS
              THRU 986-RESET-KEYS-EXIT
FCAMH *    PERFORM 996-COMMIT
FCAMH *       THRU 996-COMMIT-EXIT.

UCSDX      MOVE '$$ALL' TO DBLINK-CURSOR-NAME-WORK.
           PERFORM 991-CLOSE-CURSORS
UCSDX         THRU 991-CLOSE-CURSORS-EXIT.

           PERFORM 999-SQL-STATUS
              THRU 999-SQL-STATUS-EXIT.
005400     GOBACK.                                                      00540000
005500*************************************************************     00550000
005600*COPY IDMS IDMS-STATUS.                                           00560000
005700************************************************************      00570000
005800 IDMS-ABORT SECTION.                                              00580000
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                   900-START-SUBROUTINES                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       900-START-SUBROUTINES SECTION.
      *    CONVERT/DB SUBROUTINES FOLLOW
       900-START-SUBROUTINES-EXIT.
           EXIT.

      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                   906-05-R8005 ROUTINE                        *
      *%                                                               *
      *%-------------------------------------------------------------- *
       906-05-R8005.
           MOVE '906-05-R8005' TO DBLINK-CURR-PARAGRAPH.
           MOVE 'R8005' TO ERROR-RECORD.
           MOVE ZERO TO ERROR-STATUS.
           EXEC SQL
               SELECT
                  USER_CD
                 ,LAST_ACTVY_DT
                 ,UNVRS_CD
                 ,SCRTY_USER_CD
                 ,SHORT_NAME
                 ,FULL_NAME
                 ,SSA_FLAG
                 ,USA_FLAG
                 ,SCRTY_UNIT
                 ,FULL_NAME_UPPER
                 ,YTH_SCRN_SETF
                 ,FK1_YTH_SCRTY_UNIT
                 ,FK1_YTH_TMPLT_CD
                 ,YTH_ELEM_SETF
                 ,FK2_YTH_SCRTY_UNIT
                 ,FK2_YTH_TMPLT_CD
                 ,YTH_CONT_SETF
                 ,FK3_YTH_SCRTY_UNIT
                 ,FK3_YTH_TMPLT_CD
                 ,INDX_TS
               INTO
                  :YUS-USER-CD
                 ,:YUS-LAST-ACTVY-DT
                 ,:YUS-UNVRS-CD
                 ,:YUS-SCRTY-USER-CD
                 ,:YUS-SHORT-NAME
                 ,:YUS-FULL-NAME
                 ,:YUS-SSA-FLAG
                 ,:YUS-USA-FLAG
                 ,:YUS-SCRTY-UNIT
                 ,:YUS-FULL-NAME-UPPER
                 ,:YUS-YTH-SCRN-SETF
                 ,:YUS-FK1-YTH-SCRTY-UNIT
                 ,:YUS-FK1-YTH-TMPLT-CD
                 ,:YUS-YTH-ELEM-SETF
                 ,:YUS-FK2-YTH-SCRTY-UNIT
                 ,:YUS-FK2-YTH-TMPLT-CD
                 ,:YUS-YTH-CONT-SETF
                 ,:YUS-FK3-YTH-SCRTY-UNIT
                 ,:YUS-FK3-YTH-TMPLT-CD
                 ,:YUS-INDX-TS
               FROM YUS_SEC_USR_V
               WHERE SCRTY_USER_CD = :YUS-SCRTY-USER-CD
           END-EXEC.
           IF SQLCODE = +100
               EVALUATE DBLINK-TYPE-GET
                   WHEN DBLINK-CALC
                       MOVE DBLINK-NOT-FOUND TO ERROR-STATUS
                   WHEN DBLINK-CURR
                       MOVE DBLINK-NO-CURRENCY TO ERROR-STATUS
                   WHEN DBLINK-DBKEY
                       MOVE DBLINK-DBKEY-ERROR TO ERROR-STATUS
                   WHEN OTHER
                       MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
               END-EVALUATE
               GO TO 906-05-R8005-EXIT
           END-IF.
           IF SQLCODE NOT = 0
               MOVE DBLINK-DB2-ERROR TO ERROR-STATUS
               GO TO 906-05-R8005-EXIT
           END-IF.
           MOVE DBLINK-R8005 TO DBLINK-RECORD-MADE-CURRENT.
           MOVE ' ' TO DBLINK-SET-NAME-WORK.
           MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.
           PERFORM 985-CURR-R8005
              THRU 985-CURR-R8005-EXIT.
           IF ERROR-STATUS = DBLINK-DB2-ERROR
               GO TO 906-05-R8005-EXIT
           END-IF.
           IF DBLINK-TYPE-SELECT = 'O'
               PERFORM 975-TO-R8005
                  THRU 975-TO-R8005-EXIT
           END-IF.
       906-05-R8005-EXIT.
           EXIT.

      *%****************************** *
      *% 975-TO-R8005 ROUTINE
      *%****************************** *
      *%   COPY CB758005.
CPYNME     COPY HVIDYUS.

      *%****************************** *
      *% 976-FROM-R8005 ROUTINE
      *%****************************** *
      *%   COPY CB768005.
CPYNME     COPY IDHVYUS.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  985-CURR-R8005 ROUTINE                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       985-CURR-R8005.
           MOVE '985-CURR-R8005' TO DBLINK-CURR-PARAGRAPH.

      *%****************************** *
      *%INITIALIZE DBLINK SUBSCHEMA-CTRL
      *%****************************** *
           MOVE ZERO TO ERROR-STATUS.
           MOVE SPACES TO ERROR-AREA.
           MOVE SPACES TO ERROR-SET.
           MOVE SPACES TO ERROR-RECORD.
           MOVE 'R8005' TO RECORD-NAME.
           MOVE 'F-SCTY' TO AREA-NAME.

      *%****************************** *
      *%CLOSE ALL RECORD-RELATED CURSORS
      *%****************************** *
           MOVE DBLINK-R8005 TO DBLINK-RECORD-MADE-CURRENT.
           MOVE ' ' TO DBLINK-CURSOR-NAME-WORK.
           IF ERROR-STATUS = DBLINK-DB2-ERROR
               GO TO 985-CURR-R8005-EXIT
           END-IF.
           MOVE ' ' TO DBLINK-RECORD-MADE-CURRENT.

      *%****************************** *
      *%SET RECORD KEY
      *%****************************** *
           IF DBLINK-TYPE-GET NOT = DBLINK-CURR
               MOVE YUS-SCRTY-USER-CD TO DBLINK-R8005-01
           END-IF.

      *%****************************** *
      *%SET CURRENT OF RUN-UNIT
      *%****************************** *
           MOVE DBLINK-R8005 TO DBLINK-VIEW-NAME-CURRENT.
           MOVE DBLINK-R8005-KEY TO DBLINK-VIEW-200-CURRENT.

      *%****************************** *
      *%SET KEYS FOR SETS WHICH RECORD OWNS
      *%****************************** *

      *%****************************** *
      *%SET KEYS FOR SETS IN WHICH RECORD IS MEMBER
      *%****************************** *
       985-CURR-R8005-EXIT.
           EXIT.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  986-RESET-KEYS ROUTINE                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       986-RESET-KEYS.
           MOVE '986-RESET-KEYS' TO DBLINK-CURR-PARAGRAPH.
           MOVE SPACES TO DBLINK-RECORD-KEYS.
           MOVE LOW-VALUES TO DBLINK-SET-KEYS.
       986-RESET-KEYS-EXIT.
           EXIT.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                 991-CLOSE-CURSORS ROUTINE                     *
      *%                                                               *
      *%-------------------------------------------------------------- *
       991-CLOSE-CURSORS.
       991-CLOSE-CURSORS-EXIT.
           EXIT.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                    996-COMMIT ROUTINE                         *
      *%                                                               *
      *%-------------------------------------------------------------- *
       996-COMMIT.
           MOVE '996-COMMIT' TO DBLINK-CURR-PARAGRAPH.
           EXEC SQL
               COMMIT WORK
           END-EXEC.
       996-COMMIT-EXIT.
           EXIT.
      /
      *%-------------------------------------------------------------- *
      *%                                                               *
      *%                  999-SQL-STATUS ROUTINE                       *
      *%                                                               *
      *%-------------------------------------------------------------- *
       999-SQL-STATUS.
           EXEC SQL
               INCLUDE BTCHERR
           END-EXEC.
       999-SQL-STATUS-EXIT.
           EXIT.

